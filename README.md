---
Copyright &copy; 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.



**NOTE: THIS README WILL CHANGE WITH NEW UPDATES AND FILES UNTIL THE FINAL PROJECT**

> **_Starting with Keycloack_**

This link will send you to a wiki page for the guide that you need when you start your keycloack for the first time.

[https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/reportingsystem/keycloak-configuartion/-/wikis/Starting-with-Keycloak](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/reportingsystem/keycloak-configuartion/-/wikis/Starting-with-Keycloak)


> **_Securing the example frontend with keycloack_**

- First clone the example frontend. The link to the frontend example:

[https://gitlab.com/LibreFoodPantry/training/spikeathons/winter-2021/stoney-manage-items/frontend)](https://gitlab.com/LibreFoodPantry/training/spikeathons/winter-2021/stoney-manage-items/frontend))

- Edit the index.html and script.js to add the right code to connect your keycloack with the frontend. The edit files are provided in this project. replace the original index and script with the updated ones. Add in the src folder the keycloack.json file. 

- Run the frontend docker container with one of the four options that are provided to you in the readme.md in the frontend folder. I recommend to run using a development web server, the backend server, and its database or Run using a development web server and a mock server for the backend. In the same time you need to run the keycloack docker container. 

- Go to [http://localhost:8080](http://localhost:8080) and check the result.

> **_Keycloack Backend_**

- The updated index_1.html is the newest file that will work for both the backend and frontend configuration. I left the index.html in case you want to test and try just the log in part. The new index file must be together with the keycloakCommands file to work. The files should be at the same level.

- **To initialize Keycloack**

-docker run --name keycloak --net keycloak-network -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -p 8080:8080 jboss/keycloak

- **To initialize Keycloack backend**

-docker run --name mysql -d --net keycloak-network -e MYSQL_DATABASE=keycloak -e MYSQL_USER=keycloak -e MYSQL_PASSWORD=password -e MYSQL_ROOT_PASSWORD=root_password mysql

Both will be on the "keycloak-network" network by default.

