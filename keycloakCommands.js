var keycloak = new Keycloak();

    console.log("Keycloak started.");
    keycloak.init({ onLoad: 'login-required' })
        .success(function(authenticated) {
            console.log(authenticated ? 'authenticated' : 'not authenticated');
            console.log(keycloak);
            document.getElementById('user').innerHTML = "Logged in as " + keycloak.idTokenParsed.name + ", " + keycloak.idTokenParsed.email;
            document.getElementById('bearer').innerHTML = "Bearer token: " + keycloak.token;
        })
        .error(function() {
            console.log('failed to initialize');
        });

function refreshToken(minValidity) {
    keycloak.updateToken(minValidity).then(function(refreshed) {
      if (refreshed) {
          output(keycloak.tokenParsed);
      } else {
          output('Token not refreshed, valid for ' + Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
      }
      }).catch(function() {
          keycloak.logout();
          output('Failed to refresh token');
      });
      console.log("Checked refresh token");
 }
 
 //Link to info about getting token info with REST
 //https://developers.redhat.com/blog/2020/01/29/api-login-and-jwt-token-generation-using-keycloak/